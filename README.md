# WebSocket Slider Information

This application was written to create a browser-based, graphical element with a fast refresh rate, based on real-time date accessed from the server

The client can be either on localhost or remote

## Features
 - SVG graphics smoothly and precisely scale to the screen size
 - Fast refresh rate: up to 231 fps observed
 - The server can execute on an extemely minimal embedded board (Gumstix, Zedboard, Raspberry Pi, PicoTux, etc.)

## Install Notes (Gentoo Linux)
 - requires Python 2.7
 - /etc/portage/package.use/python
 - /etc/portage/package.keywords/python
    =dev-python/gevent-websocket-0.9.3 ~amd64
 - emerge bottle gevent-websocket -av
    dev-python/greenlet doc
    dev-python/gevent doc examples

## Tested on

The client has been tested on:
  - Firefox for Windows, Linux, and Android 4.3 (works perfectly)
  - Chrome for Linux and Windows (works perfectly)
  - Safari for Mac and IOS (second-hand information indicates positive results)
  - Internet Exploder (certifiably-not-compatible)

### Some version specifics:
 - Chromium 40.0.2214.111 (00:45:32 02/13/15) on 64-bit Gentoo Linux
    [I] www-client/chromium
         Available versions:  40.0.2214.91^d ~40.0.2214.93^d 40.0.2214.111^d ~41.0.2272.12-r1^d ~41.0.2272.16^d ~41.0.2272.35^d [M]~42.0.2288.6^d [M]~42.0.2292.0^d {bindist cups custom-cflags gnome gnome-keyring hidpi kerberos neon pic pulseaudio selinux +tcmalloc test widevine LINGUAS="+am +ar +bg +bn +ca +cs +da +de +el +en_GB +es +es_LA +et +fa +fi +fil +fr +gu +he +hi +hr +hu +id +it +ja +kn +ko +lt +lv +ml +mr +ms +nb +nl +pl +pt_BR +pt_PT +ro +ru +sk +sl +sr +sv +sw +ta +te +th +tr +uk +vi +zh_CN +zh_TW"}
         Installed versions:  40.0.2214.111^d(00:45:32 02/13/15)(cups pic tcmalloc -bindist -custom-cflags -gnome -gnome-keyring -kerberos -neon -pulseaudio -selinux -test -widevine LINGUAS="de -am -ar -bg -bn -ca -cs -da -el -en_GB -es -es_LA -et -fa -fi -fil -fr -gu -he -hi -hr -hu -id -it -ja -kn -ko -lt -lv -ml -mr -ms -nb -nl -pl -pt_BR -pt_PT -ro -ru -sk -sl -sr -sv -sw -ta -te -th -tr -uk -vi -zh_CN -zh_TW")
         Homepage:            http://chromium.org/
         Description:         Open-source version of Google Chrome web browser
 - Firefox 31.3.0 (17:33:42 04.01.2015) on 32-bit Gentoo Linux
    [I] www-client/firefox
        Available versions:  *10.0.11 24.3.0 24.8.0 31.3.0 ~31.4.0 ~35.0 {+alsa bindist custom-cflags custom-optimization (+)dbus debug +gmp-autoupdate gstreamer hardened +ipc +jit libnotify +minimal pgo pulseaudio selinux startup-notification system-cairo system-icu system-jpeg system-libvpx system-sqlite test +webm wifi LINGUAS="af ak ar as ast be bg bn_BD bn_IN br bs ca cs csb cy da de el en_GB en_ZA eo es_AR es_CL es_ES es_MX et eu fa fi fr fy_NL ga_IE gd gl gu_IN he hi_IN hr hu hy_AM id is it ja kk km kn ko ku lg lt lv mai mk ml mr nb_NO nl nn_NO nso or pa_IN pl pt_BR pt_PT rm ro ru si sk sl son sq sr sv_SE ta ta_LK te th tr uk vi xh zh_CN zh_TW zu"}
        Installed versions:  31.3.0(17:33:42 04.01.2015)(dbus gstreamer jit minimal pulseaudio -bindist -custom-cflags -custom-optimization -debug -hardened -pgo -selinux -startup-notification -system-cairo -system-icu -system-jpeg -system-libvpx -system-sqlite -test -wifi LINGUAS="de -af -ar -as -ast -be -bg -bn_BD -bn_IN -br -bs -ca -cs -csb -cy -da -el -en_GB -en_ZA -eo -es_AR -es_CL -es_ES -es_MX -et -eu -fa -fi -fr -fy_NL -ga_IE -gd -gl -gu_IN -he -hi_IN -hr -hu -hy_AM -id -is -it -ja -kk -km -kn -ko -ku -lt -lv -mai -mk -ml -mr -nb_NO -nl -nn_NO -or -pa_IN -pl -pt_BR -pt_PT -rm -ro -ru -si -sk -sl -son -sq -sr -sv_SE -ta -te -th -tr -uk -vi -xh -zh_CN -zh_TW -zu")
        Homepage:            http://www.mozilla.com/firefox
        Description:         Firefox Web Browser
 - Firefox 35.0.1 Android 4.3 on Samsung S3 GT-I9300

## Credits

The arrow was obtained from http://www.clker.com/clipart-3548.html

The ruler was obtained from https://openclipart.org/detail/189197/testigo-m%C3%83%C2%A9trico:-cent%C3%83%C2%ADmetros-verticales-by-andresmoya-189197

Most of the code was written by Bryant Hansen, but various pieces of code were copy-pasted from various examples.  Anything that was written by me can be considered licensed under GPLv3, but don't assume that the entire code base is GPLv3.  Hopefully it serves as a useful example for someone.


## References

### General Javascript & HTML

 - http://www.w3schools.com/js/js_examples.asp
 - http://www.w3schools.com/js/js_input_examples.asp
 - http://www.w3schools.com/js/js_dom_examples.asp
 - http://www.w3schools.com/html/html_examples.asp

### WebSockets
 - http://www.ibm.com/developerworks/library/wa-reverseajax2/

### html svg compatibility
 - http://www.w3schools.com/svg/svg_inhtml.asp
 - http://caniuse.com/#feat=svg-img
 - http://css-tricks.com/using-svg/
 - http://www.w3schools.com/html/html5_svg.asp

### css background stretch svg
 - https://developer.mozilla.org/en-US/docs/Web/CSS/Scaling_of_SVG_backgrounds

### CSS
 - http://www.w3schools.com/cssref/css_selectors.asp

### html scale div to bottom of the page
 - http://dabblet.com/gist/2590942
 - http://stackoverflow.com/questions/1495407/css-a-way-to-maintain-aspect-ratio-when-resizing-a-div
 - http://jsbin.com/eqeseb/2/edit?html,css,output
 - http://dabblet.com/gist/2590942

### Javascript/Ajax Polling
 - http://webcooker.net/ajax-polling-requests-php-jquery/
 - http://techoctave.com/c7/posts/60-simple-long-polling-example-with-javascript-and-jquery
 - http://stackoverflow.com/questions/6835835/jquery-simple-polling-example
 - http://blog.falafel.com/polling-ajax-requests-in-javascript/
 - http://www.w3schools.com/jsref/met_win_settimeout.asp

### FPS
 - http://codetheory.in/controlling-the-frame-rate-with-requestanimationframe/
 - http://www.html5gamedevs.com/topic/1828-how-to-calculate-fps-in-plain-javascript/
 - https://www.google.ch/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=0CDEQFjAC&url=http%3A%2F%2Fcodetheory.in%2Fcontrolling-the-frame-rate-with-requestanimationframe%2F&ei=cqTgVNvWOIS4UaHygMAO&usg=AFQjCNH6DQwudizKGlmhFR047OQUYheEew&bvm=bv.85970519,d.d24&cad=rja
 - https://www.codeaurora.org/blogs/mbapst/measuring-fps-web
 - http://stackoverflow.com/questions/5078913/html5-canvas-performance-calculating-loops-frames-per-second

### ajax websocket example
 - http://wiki4.caucho.com/Understanding_WebSockets_versus_Ajax/REST_for_Java_EE_Developers
 - https://www.google.ch/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=0CDsQFjAD&url=http%3A%2F%2Fwww.ibm.com%2Fdeveloperworks%2Flibrary%2Fwa-reverseajax2%2F&ei=yUffVIjjDsOsUcqTgOgN&usg=AFQjCNGGZAsmoHtwVbgptGPYFmsJXRa31g&bvm=bv.85970519,d.d24&cad=rja

### ajax long polling example
 - http://webcooker.net/ajax-polling-requests-php-jquery/
 - https://gist.github.com/jasdeepkhalsa/4353139
 - http://rmurphey.com/blog/2013/02/04/refactoring-setInterval-polling/
 - https://thomasgriffin.io/handy-javascript-polling-function-for-resize-and-other-events/
 - http://techoctave.com/c7/posts/60-simple-long-polling-example-with-javascript-and-jquery

### ajax polling rate
 - http://developers.cogentrts.com:8080/DH_ajax_3.asp
 - view-source:http://developers.cogentrts.com:8080/DH_ajax_3.asp
 - http://developers.cogentrts.com:8080/scripts/livedata.js
 - http://developers.cogentrts.com:8080/css/dhwebserver.css
 - http://www.sencha.com/forum/showthread.php?78461-Best-Practices-for-High-Frequency-AJAX-Polling
 - http://www.sencha.com/forum/showthread.php?66788-3.0-rc1-Chart-with-Long-Polling
 - http://reallifejs.com/brainchunks/repeated-events-timeout-or-interval/
 - http://www.w3schools.com/js/js_timing.asp

### apache wsgi polling rate
 - Primer to Asynchronous Applications http://bottlepy.org/docs/dev/async.html
 - http://mrjoes.github.io/2013/06/21/python-realtime.html
 - http://dabblet.com/gist/2590942

### Layout
 - http://www.thenoodleincident.com/tutorials/box_lesson/boxes.html
 - http://code.tutsplus.com/tutorials/html-5-and-css-3-the-techniques-youll-soon-be-using--net-5708
 - http://viljamis.com/blog/2012/scaling-with-em-units/
 - http://stackoverflow.com/questions/8795764/auto-adjust-div-height-with-css
 - http://dabblet.com/gist/2590942
 - http://stackoverflow.com/questions/712689/css-div-stretch-100-page-height
 - http://css-tricks.com/almanac/properties/d/display/
 - http://www.w3schools.com/cssref/pr_class_display.asp
 - http://css-tricks.com/almanac/properties/d/display/
 - http://dev.w3.org/csswg/css-flexbox/
 - http://colintoh.com/blog/display-table-anti-hero
 - http://www.sitepoint.com/adapting-your-site-to-different-window-sizes/
 - http://stackoverflow.com/questions/14421554/css-scaling-positioned-elements-rel-to-browser-window-size
 - http://stackoverflow.com/questions/16217355/automatically-resize-images-with-browser-size-using-css
 - http://stackoverflow.com/questions/15588091/how-to-have-css-background-image-scale-with-window-size-and-device

### CPU Usage
 - http://man7.org/linux/man-pages/man5/proc.5.html
 - http://books.gigatux.nl/mirror/linuxperformanceguide/0131486829/ch02lev1sec2.html
 - http://phoxis.org/2013/09/05/finding-overall-and-per-core-cpu-utilization/
 - https://www.kernel.org/doc/Documentation/filesystems/proc.txt
 - http://linux.die.net/man/5/proc

### Fonts
 - http://en.wikipedia.org/wiki/Font_family_(HTML)
 - http://www.w3schools.com/cssref/css_websafe_fonts.asp
 - http://www.cssfontstack.com/

### Javascript animation
 - http://stackoverflow.com/questions/26819909/css-animation-keyframe-with-smooth-movement
 - Very good example: http://jsfiddle.net/ww31468f/
