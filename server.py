#!/usr/bin/env python2.7
# from:
#   http://bottlepy.org/docs/dev/async.html

import sys
from datetime import datetime
from bottle import request, Bottle, abort
from math import sin, pi
from time import sleep
from get_cpu_usage import get_cpu_usage

'''
@brief LISTEN_IP
set this to "localhost or 127.0.0.1 for local connections
set it to 0.0.0.0 to listen for connections from all remote systems
it's probably possible to select various subnets and/or physical interfaces
'''
LISTEN_IP = "0.0.0.0"
PORT = 8080

app = Bottle()


class CpuCore:
    def __init__(self, cpu):
        self.cpu = cpu
        self.user = 0
        self.nice = 0
        self.system = 0
        self.idle = 0
        self.iowait = 0

        self.d_user = 0
        self.d_nice = 0
        self.d_system = 0
        self.d_idle = 0
        self.d_iowait = 0

cpuCores = {}


def TRACE(msg):
    # get the name of the calling function: str(stack()[2][3])
    calling_function = ""
    try:
        calling_function = stack()[1][3]
    except:
        pass
    m = "%s :%s: %s\n" % (
        datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3],
        calling_function,
        msg
    )
    sys.stderr.write(m)

def get_message():
    '''
    usage = get_cpu_usage()
    sleep(0.1)
    usage = get_cpu_usage()
    TRACE("USAGE: %6.2f" % usage)
    #return usage
    '''
    t = datetime.now().strftime("%s.%f")[0:-3]
    t2 = float(t) % (2 * pi)
    t3 = sin(t2 * 2) * 5 + 5
    t3 = sin(t2) * sin(t2 * 2) * 5 + 5
    t3 = sin(t2) * sin(t2 * 3) * sin(t2 * 5) * 5 + 5
    t3 = sin(t2) * sin(t2 * 3) * sin(t2 * 5) * sin(t2 * 7) * sin(t2 * 9) * 5 + 5
    s1 = str(t3)
    return s1

@app.route('/websocket')
def handle_websocket():
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
        abort(400, 'Expected WebSocket request.')

    while True:
        try:
            #TRACE("checking for incoming wsock message")
            message = wsock.receive()
            m = get_message()
            # wsock.send("Your message was: %r" % message)
            #TRACE("returning message '%s'" % m)
            wsock.send("%s" % m)
        except WebSocketError:
            break

from gevent.pywsgi import WSGIServer
from geventwebsocket import WebSocketError
from geventwebsocket.handler import WebSocketHandler
server = WSGIServer((LISTEN_IP, PORT), app,
                    handler_class=WebSocketHandler)
server.serve_forever()

