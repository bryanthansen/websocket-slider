#!/usr/bin/python3 -u
'''
# Bryant Hansen
'''


from time import sleep

def TRACE(msg):
    # get the name of the calling function: str(stack()[2][3])
    calling_function = ""
    try:
        calling_function = stack()[1][3]
    except:
        pass
    m = "%s :%s: %s\n" % (
        datetime.now().strftime("%Y%m%d_%H%M%S.%f")[0:-3],
        calling_function,
        msg
    )
    sys.stderr.write(m)


class CpuCore:
    def __init__(self, cpu):
        self.cpu = cpu
        self.user = 0
        self.nice = 0
        self.system = 0
        self.idle = 0
        self.iowait = 0

        self.d_user = 0
        self.d_nice = 0
        self.d_system = 0
        self.d_idle = 0
        self.d_iowait = 0

cpuCores = {}

def get_cpu_usage_debug():
    '''
        Sample output from /proc/stat:
        cpu  56914 1145 11589 1630927 17834 0 91 0 0 0
        cpu0 8580 474 1935 198653 4948 0 25 0 0 0
    '''
    f = open('/proc/stat')
    usage_data = []
    print("\n%5s %10s %10s %10s %10s %10s" % (
            "CPU",
            "user",
            "nice",
            "system",
            "idle",
            "wait",
    ))
    for line in open("/proc/stat",'r'):
        if line.startswith("cpu"):
            info = line.split()
            cpu = info[0]
            if not cpu in cpuCores.keys():
                cpuCores[cpu] = CpuCore(cpu)
            cpuCores[cpu].d_user   = int(info[1]) - cpuCores[cpu].user;
            cpuCores[cpu].user     = int(info[1])
            cpuCores[cpu].d_nice   = int(info[2]) - cpuCores[cpu].nice;
            cpuCores[cpu].nice     = int(info[2])
            cpuCores[cpu].d_system = int(info[3]) - cpuCores[cpu].system
            cpuCores[cpu].system   = int(info[3])
            cpuCores[cpu].d_idle   = int(info[4]) - cpuCores[cpu].idle
            cpuCores[cpu].idle     = int(info[4])
            cpuCores[cpu].d_iowait = int(info[5]) - cpuCores[cpu].iowait
            cpuCores[cpu].iowait   = int(info[5])
            #d_irq = int(info[6]) - irq
            #irq = int(info[6])
            #softirq = int(info[7])
            #steal = int(info[8])
            #guest = int(info[9])
            usage_data.append(' '.join(info[0:2]))
            print("%5s %10s %10s %10s %10s %10s" % (
                  str(cpu),
                  str(cpuCores[cpu].d_user),
                  str(cpuCores[cpu].d_nice),
                  str(cpuCores[cpu].d_system),
                  str(cpuCores[cpu].d_idle),
                  str(cpuCores[cpu].d_iowait),
            ))

def get_cpu_usage():
    usage_pcnt = 0.0
    try:
        for line in open("/proc/stat",'r'):
            line = line.strip()
            if line.startswith("cpu "):
                info = line.split()
                cpu = info[0]
                if not cpu in cpuCores.keys():
                    cpuCores[cpu] = CpuCore(cpu)
                cpuCores[cpu].d_user   = int(info[1]) - cpuCores[cpu].user;
                cpuCores[cpu].user     = int(info[1])
                cpuCores[cpu].d_nice   = int(info[2]) - cpuCores[cpu].nice;
                cpuCores[cpu].nice     = int(info[2])
                cpuCores[cpu].d_system = int(info[3]) - cpuCores[cpu].system
                cpuCores[cpu].system   = int(info[3])
                cpuCores[cpu].d_idle   = int(info[4]) - cpuCores[cpu].idle
                cpuCores[cpu].idle     = int(info[4])
                cpuCores[cpu].d_iowait = int(info[5]) - cpuCores[cpu].iowait
                cpuCores[cpu].iowait   = int(info[5])
                total_usage = cpuCores[cpu].d_user + cpuCores[cpu].d_nice + cpuCores[cpu].d_system;
                total = cpuCores[cpu].d_user + cpuCores[cpu].d_nice + cpuCores[cpu].d_system + cpuCores[cpu].d_idle;
                usage_pcnt = float(total_usage) / total * 100.0;
    except:
        TRACE("Exception %s in get_cpu_usage()" % str(sys.exc_info))
    return usage_pcnt


############################################
if __name__ == '__main__':
    # Main
    usage = get_cpu_usage()
    sleep(0.1)
    usage = get_cpu_usage()
    print("%6.2f" % usage)

